package com.test.juliya.foremnotion.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.test.juliya.foremnotion.R;
import com.test.juliya.foremnotion.controller.LocationService;
import com.test.juliya.foremnotion.fragment.MainFragment;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (checkConnection()) {
            startApp();
        } else {
            showDialog();
        }

    }

    private boolean checkConnection() {
        LocationService locationService = LocationService.getLocationInstance(getApplicationContext());
        if (locationService.checkPermissions() && locationService.checkGPSConnection()
                && locationService.isOnline())
            return true;
        else
            return false;
    }


    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Check your Internet connection, GPS and restart the app, please.");
        builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.create().show();
    }


    private void startApp() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, MainFragment.getInstance()).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
